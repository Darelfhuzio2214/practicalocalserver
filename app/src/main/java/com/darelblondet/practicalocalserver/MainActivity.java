package com.darelblondet.practicalocalserver;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.darelblondet.practicalocalserver.adapter.AuthenticationAdapter;
import com.darelblondet.practicalocalserver.adapter.FoodAdapter;
import com.darelblondet.practicalocalserver.model.Authentication;
import com.darelblondet.practicalocalserver.model.Food;
import com.darelblondet.practicalocalserver.model.FoodByCityNameResponse;
import com.darelblondet.practicalocalserver.model.Login;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private void login(){
        AuthenticationAdapter authenticationAdapter = new AuthenticationAdapter();
        Login login = new Login("prueba@prueba.com", "12345678");
        Call<Authentication> call = authenticationAdapter.getUsers(login);
        call.enqueue(new Callback<Authentication>() {
            @Override
            public void onResponse(Call<Authentication> call, Response<Authentication> response) {
                //Verificar mas tarde XD
                //token = response.body().getAccessToken()
                Log.e("Tok", response.body().getAccessToken());
            }

            @Override
            public void onFailure(Call<Authentication> call, Throwable t) {

            }
        });

    }

    private void getFoodByCityname(){
        FoodAdapter foodAdapter = new FoodAdapter();
        Call<FoodByCityNameResponse> call = foodAdapter.getFoodByCityName(
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjU2NjIxODAzLCJleHAiOjE2NTY3MDgyMDN9.2rt7q9l1VgOeWeeffEEh4-5Onw750PUcTM1QrISsrXAeyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjU2NjIxODAzLCJleHAiOjE2NTY3MDgyMDN9.2rt7q9l1VgOeWeeffEEh4-5Onw750PUcTM1QrISsrXA",
                "Manta");

        call.enqueue(new Callback<FoodByCityNameResponse>() {
            @Override
            public void onResponse(Call<FoodByCityNameResponse> call, Response<FoodByCityNameResponse> response) {
                List<Food> foodByCityNameResponde = response.body().getFoods();

                for (Food item:
                foodByCityNameResponde){
                    Log.e("FOOD", item.getName());
                }
            }

            @Override
            public void onFailure(Call<FoodByCityNameResponse> call, Throwable t) {
                Log.e("FOOD ERROR", t.getMessage());

            }
        });

    }






}