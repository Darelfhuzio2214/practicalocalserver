package com.darelblondet.practicalocalserver.adapter;

import com.darelblondet.practicalocalserver.contants.FoodAPI;
import com.darelblondet.practicalocalserver.model.Authentication;
import com.darelblondet.practicalocalserver.model.FoodByCityNameResponse;
import com.darelblondet.practicalocalserver.service.FoodService;

import retrofit2.Call;

public class FoodAdapter
        extends BaseAdapter
        implements FoodService {

    FoodService foodService;

    public FoodAdapter(){
        super(FoodAPI.BASE_URL);
        foodService = createService(FoodService.class);
    }

    @Override
    public Call<FoodByCityNameResponse> getFoodByCityName(
            String token, String cityName){
        return foodService.getFoodByCityName(token, cityName);
    }


}


