package com.darelblondet.practicalocalserver.adapter;

import com.darelblondet.practicalocalserver.contants.AuthenticationAPI;
import com.darelblondet.practicalocalserver.model.Authentication;
import com.darelblondet.practicalocalserver.model.Login;
import com.darelblondet.practicalocalserver.service.AuthenticationService;

import retrofit2.Call;

public class AuthenticationAdapter extends BaseAdapter implements AuthenticationService {
    private AuthenticationService authenticationService;

    public AuthenticationAdapter(){
        super(AuthenticationAPI.BASE_URL);
        authenticationService = createService(AuthenticationService.class);
    }

    @Override
    public Call<Authentication> getUsers(Login login){
        return authenticationService.getUsers(login);
    }

}
