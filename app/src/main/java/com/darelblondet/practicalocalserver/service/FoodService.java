package com.darelblondet.practicalocalserver.service;

import com.darelblondet.practicalocalserver.contants.FoodAPI;
import com.darelblondet.practicalocalserver.model.FoodByCityNameResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface FoodService {
    @GET(FoodAPI.FOOD_ENDPOINT)
    Call<FoodByCityNameResponse> getFoodByCityName(
            @Header("x-access-token") String token,
            @Path("cityname") String cityName);


}
