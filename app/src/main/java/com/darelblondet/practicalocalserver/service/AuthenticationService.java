package com.darelblondet.practicalocalserver.service;

import com.darelblondet.practicalocalserver.contants.AuthenticationAPI;
import com.darelblondet.practicalocalserver.model.Authentication;
import com.darelblondet.practicalocalserver.model.Login;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AuthenticationService {

    @POST(AuthenticationAPI.AUTHENTIFICATION_ENDPOINT)
    Call<Authentication> getUsers(@Body Login login);

}
