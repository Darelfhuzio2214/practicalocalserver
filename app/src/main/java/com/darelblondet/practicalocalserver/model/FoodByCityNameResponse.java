package com.darelblondet.practicalocalserver.model;

import java.util.List;

public class FoodByCityNameResponse {
    private boolean status;
    private List<Food> foods;

    public boolean getStatus(){return status; }
    public void setStatus(boolean value) {
        this.status = value;}

    public List<Food> getFoods() {return foods; }
    public void setFoods(List<Food> value) {
        this.foods = value;
    }
}

