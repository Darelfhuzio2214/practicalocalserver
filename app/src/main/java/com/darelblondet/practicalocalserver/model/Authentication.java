package com.darelblondet.practicalocalserver.model;

import com.google.gson.annotations.SerializedName;

public class Authentication {

    @SerializedName("id")
    private  long id;

    @SerializedName("name")
    private  String name;

    @SerializedName("last_name")
    private  String last_name;

    @SerializedName("email")
    private  String email;

    @SerializedName("photo")
    private  String photo;

    @SerializedName("accessToken")
    private  String accessToken;

    public long getId() {return id;}
    public void setId(long value) {this.id = value;}

    public String getName() {return name;}
    public void setName(String value) {this.name = value;}

    public String getLast_name() {return last_name;}
    public void setLast_name(String value) {this.last_name = value;}

    public String getEmail() {return email;}
    public void setEmail(String value) {this.email = value;}

    public String getPhoto() {return photo;}
    public void setPhoto(String value) {this.photo = value;}

    public String getAccessToken() {return accessToken;}
    public void setAccessToken(String accessToken) {this.accessToken = accessToken;}
}
