package com.darelblondet.practicalocalserver.model;

import com.google.gson.annotations.SerializedName;

public class Food {

    @SerializedName("id")
    private  long id;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private  String description;

    @SerializedName("pvp")
    private String pvp;

    @SerializedName("city")
    private  String city;

    @SerializedName("userID")
    private long userID;

    public long getId() {return id;}
    public void setId(long value) {this.id = value;}

    public String getName() {return name;}
    public void setName(String value) {this.name = value;}

    public String getDescription() {return description;}
    public void setDescription(String value) {this.description = value;}

    public String getPvp() {return pvp;}
    public void setPvp(String value) {this.pvp = value;}

    public String getCity() {return city;}
    public void setCity(String value) {this.city = value;}

    public long getUserID() {return userID;}
    public void setUserID(long value) {this.userID = value;}
}
