package com.darelblondet.practicalocalserver.contants;

public class FoodAPI {

    public static final String BASE_URL = "http://10.0.2.2:8080/api";

    public static final String FOOD_ENDPOINT = "foodByCity/{cityname}";
}
